@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card bg-primary text-white border-primary">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h2>Edit Questions</h2>
                            <div class="ml-auto">
                                <a href="{{ route('questions.index') }}" class="btn btn-outline-secondary">Back All Questions</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body bg-white text-dark">
                        <form action="{{ route('questions.update',$question->id) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="question-title">Question Title</label>
                                <input type="text" value="{{ $question->title }}" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" name="title" id="question-title">
                                @if($errors->has('title'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="question-title">Explain Your Question</label>
                                <textarea name="body" id="question-body" class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}" rows="10">{{ $question->title }}</textarea>
                                @if($errors->has('body'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <button class="btn btn-outline-success" type="submit">Edit Your Question</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
